<?php

// print variable
function prin() {
	$backtrace = debug_backtrace();
	echo "<hr>";
	echo "<b>".trace_format($backtrace[0])."</b>";
	call_user_func_array('prin_format', func_get_args());

	exit;
}

function prins() {
	$backtrace = debug_backtrace();
	echo "<hr>";
	echo "<b>".trace_format($backtrace[0])."</b>";
	call_user_func_array('prin_format', func_get_args());
}

// format view for backtrace file
function trace_format($trace) {
	if (isset($trace['file']))
		return str_replace(realpath('sites/all/modules'), null, str_replace('\\', '/', $trace['file'])).' line: '.$trace['line'];
}

// format view for printing variable
function prin_format() {
	echo "<pre>";
	$arg = func_get_args();

	foreach ($arg as $val) {
		echo "<hr>";
		var_dump($val);
	}
	echo "</pre>";
}

// get argument list as array
function get_arg($arg) {
	$res = array();

	if (count($arg)) {
		foreach ($arg as $val) {
			if (is_array($val)) 
				$res[] = '{'.implode(', ', get_arg($val)).'}';
			else if (is_object($val)) 
				$res[] = get_class($val);
			else if (is_string($val))
				$res[] = '"'.$val.'"';
			else if ($val == NULL)
				$res[] = 'NULL';
			else
				$res[] = $val;
		}
	}

	return $res;
}

// backtracing file
function trace() {
	$backtrace = debug_backtrace();

	foreach ($backtrace as $obj) {
		$arg = array();
		$str[] = trace_format($obj).' <b>'.@$obj['class'].'->'.$obj['function'].'('.implode(', ', get_arg($obj['args'])).')</b>';
	}

	echo "<div><li>".implode("</li><li>", $str)."</li></div>";
	call_user_func_array('prin_format', func_get_args());
}