<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>PHP - Sandbox</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Styles -->
		<link rel="stylesheet" href="resource/css/bootstrap.min.css" />
		<link rel="stylesheet" href="resource/css/style.css" />
	</head>

	<body>
		<div id="content">
			<div id="menu-bar"> 
				<button 
					type="button" 
					id="run-code" 
					class="btn btn-sm btn-primary" 
					data-placement="right" 
					title="Ctrl + S">
					<i class="glyphicon glyphicon-play"></i> Run
				</button>

				<div id="logo" class=" pull-right cursor-default dropdown"> 
					<a href="#" class="logo-label dropdown-toggle" data-toggle="dropdown" >
						PHP <span class="caret"></span>
					</a>

					<ul class="dropdown-menu">
						<li>
							<a href="?reset">
								<i class="glyphicon glyphicon-retweet"></i> Reset
							</a>
						</li>
					</ul>
				</div>
			</div>

			<div id="editor" class="row">
				<div class="col-xs-6 full-height">
					<div class="row full-height">
						<div id="code"><?php echo htmlentities($code); ?></div>
					</div>
				</div>
				<div id="result" class="col-xs-6"></div>
			</div>

			<div id="status-bar" class="cursor-default">
				<div id="status-msg" class="pull-left"> </div>
			</div>
		</div>

		<!-- Libraries -->
		<script src='resource/js/jquery.min.js'></script>
		<script src='resource/js/bootstrap.min.js'></script>
		
		<!-- Ace Editor -->
		<script src='resource/js/ace/ace.js'></script>
		<script src='resource/js/ace/ext-language_tools.js'></script>
		<script src='resource/js/ace/ext-statusbar.js'></script>

		<!-- Application -->
		<script src='resource/js/script.js'></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("[title]").tooltip();
				$('.dropdown-toggle').dropdown();
			})
		</script>
	</body>
</html>