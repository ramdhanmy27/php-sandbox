$(document).ready(function() {
    // trigger extension
    ace.require("ace/ext/language_tools");

    var editor = ace.edit("code");
    editor.session.setMode("ace/mode/php");
    editor.setTheme("ace/theme/tomorrow_night");
    editor.getSession().setUseWrapMode(true);

    // enable autocompletion and snippets
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });

    // save on ctrl+s | ctrl+r keypress
	$('#code').keydown(function(e) {
		if (e.ctrlKey && (e.which==83 || e.which==82)) {
			$.ajax({
				url : window.location.href,
				type : 'post',
				data : {code: editor.getSession().getValue()}
			});

			$('iframe').attr('src', $('iframe').attr('src'));

			e.preventDefault();
			return false;
		}
	});
});