$(document).ready(function() {
    // trigger extension
    ace.require("ace/ext/language_tools");

    var editor = ace.edit("code");
    editor.session.setMode("ace/mode/php");
    editor.setTheme("ace/theme/tomorrow_night");
    editor.getSession().setUseWrapMode(true);

    // enable autocompletion and snippets
    editor.setOptions({
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: false
    });
    
    var StatusBar = ace.require("ace/ext/statusbar").StatusBar;
    var statusBar = new StatusBar(editor, document.getElementById("status-bar"));

    (function() {
        var is_edited = false;

        var saveAndReload = function(code) {
            var data = {};

            if (typeof code !== "undefined") {
                data.code = code;
            }

            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                success: function(res) {
                    $("#result").fadeOut("fast", function() {
                        $(this).html(res).fadeIn("fast");
                    });

                    $("#status-msg").html("Saved <i class='glyphicon glyphicon-ok'></i>");
                    is_edited = false;
                }
            });
        }

        $('#code').keydown(function(e) {
            // save on ctrl+s | ctrl+r keypress
            if (e.ctrlKey && (e.which==83 || e.which==82)) {
                saveAndReload(editor.getSession().getValue());
                e.preventDefault();
            }

            if (!is_edited) {
                is_edited = true;
                $("#status-msg").html("Edited <i class='glyphicon glyphicon-asterisk'></i>");
            }
        });

        $("#run-code").click(function() {
            saveAndReload(editor.getSession().getValue());
        })

        // Display result on first load
        saveAndReload();
    }) ()
});