<?php

include 'lib/define.php';
include 'lib/debug.php';

// on save process
if (isset($_POST['code'])) {
	file_put_contents("code.php", $_POST['code']);
	exit;
}

if (isset($_GET['code'])) :
	include 'code.php';
else : ?>
	
	<head>
		<title>PHP - Sandbox</title>
		<link rel="stylesheet" href="resource/css/style.css" />
	</head>

	<body>
		<!-- code -->
		<div id='code'><?php echo htmlentities(file_get_contents('code.php')); ?></div>

		<!-- result -->
		<iframe src='<?php echo URL; ?>?code'></iframe>

		<!-- javascript -->
		<script src='resource/js/jquery-1.8.3.min.js'></script>
		<script src='resource/js/ace/ace.js'></script>
		<script src='resource/js/ace/ext-language_tools.js'></script>
		<script src='resource/js/script.js'></script>
	</body>

<?php endif; ?>