<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include 'lib/define.php';
include 'lib/debug.php';

// Cookie Handler
if (!isset($_COOKIE["id"])) {
	$id = uniqid();
	setcookie("id", $id, 2147483647, "php-sandbox");
	$filename = "./code/{$id}.php";
}
else {
	$filename = "./code/".$_COOKIE["id"].".php";
}

// Reset Source
if (isset($_GET["reset"])) {
	if (file_exists($filename)) {
		@unlink($filename);
	}

	header("location: ".dirname($_SERVER["REQUEST_URI"]));
}

// On Save
if ($_SERVER['REQUEST_METHOD'] === "POST") {
	if (isset($_POST['code'])) {
		file_put_contents($filename, $_POST['code']);
	}

	if (file_exists($filename)) {
		include $filename;
	}
}
// View Editor
else {
	if (file_exists($filename)) {
		$code = file_get_contents($filename);
	}
	else {
		$code = "<?php \n\n// You could run your code using Ctrl + S or Ctrl + R";
	}

	include './layout.php';
}